⍝ Find the biggest palindrome that is the product of two 3-digit numbers

⍝ Code (TODO : make it more efficient)
test←{⍵≡⌽⍵}{10 (⊥⍣¯1)⍵}
{⌈/(test¨⍵)/⍵}{∊⍵×¨⍳¨⍵}{((10*⍵-1)-1)+⍳((10*⍵)-(10*⍵-1))} 3

⍝ Result : 906609
