⍝ Compute the biggest prime number 

⍝ Code : 
{b←⍵⋄{⊢/(2=+⌿0=⍵∘.|⍵)/⍵}{⍵,⌽(b÷⍵)}{(0=(⍳⍵)|b)/⍳⍵}⌊b*0.5} 600851475143

⍝ Output :6857
