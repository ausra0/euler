⍝ Computes the sum of all numbers dividing 3 or 5
sumdiv3or5←{+/{⍵×⍲/[1]×↑3 5|2⍴⊆⍵}⍳⍵-1}

⍝ Test for numbers under 10 
sumdiv3or5 10 

⍝ Answer is 23
