⍝ Code 
m←↑(1 2)(2 3)⋄solve←{⊢/⍵<4E6:⍺+⊢/⍵∇(⍵+.×m)⋄⍺}

⍝ Test 
0 solve (1 2)

⍝ Output : 4613732
